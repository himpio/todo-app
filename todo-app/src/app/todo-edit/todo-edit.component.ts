import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TodoService } from '../todo.service';
import { switchMap } from 'rxjs';

@Component({
  selector: 'app-todo-edit',
  templateUrl: './todo-edit.component.html',
  styleUrls: ['./todo-edit.component.css']
})
export class TodoEditComponent implements OnInit {
  @Output() todosChanged = new EventEmitter();
  todo: any = {};

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private todoService: TodoService
  ) {}
  
  ngOnInit() {
    this.route.paramMap.pipe(
      switchMap(params => {
        const id = Number(params.get('id'));
        return this.todoService.getTodoById(id);
      })
    ).subscribe(todo => {
      this.todo = todo;
    });
  }

  onSave(): void {
    this.todoService.updateTodoById(this.todo.id, this.todo).subscribe(() => {
      this.todoService.todosChanged.emit();
      console.log('todosChanged event emitted');
      this.router.navigate(['/']);
    })
  }

  onDelete(): void {
    if (confirm('Delete this task?')) {
      this.todoService.deleteTodoById(this.todo.id).subscribe(() => {
        this.todoService.todosChanged.emit();
        console.log('todosChanged event emitted');
        this.router.navigate(['/']);
      })
    }
  }

  onCheckedChange() {
    this.todo.checked = !this.todo.checked;
    console.log(this.todo.checked);
  }

  onCancel(): void {
    this.router.navigate(['/']);
  }
}
