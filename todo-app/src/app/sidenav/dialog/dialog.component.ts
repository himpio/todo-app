import { Component, Inject, Output, EventEmitter } from '@angular/core';
import {MatDialog, MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import { TodoService } from 'src/app/todo.service';

export interface DialogData {
  id: number;
  task: string;
  checked: boolean;
}

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css']
})
export class DialogComponent {
  @Output() todosChanged = new EventEmitter<any>();

  id: number = 0;
  task: string = '';
  checked: boolean = false;

  constructor(
    public dialog: MatDialog,
    private todoService: TodoService
    ) {}
    
  openDialog(): void {
    const dialogRef = this.dialog.open(DialogComponentDialog, {
      data: {task: this.task, checked: false},
    })

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.todoService.getTodos().subscribe(todos => {
          const maxId = todos.reduce((max, todo) => todo.id > max ? todo.id : max, 0);
      
          const newTodo = {
            id: maxId + 1,
            task: result.task,
            checked: false
          };

          this.todoService.createTodo(newTodo).subscribe(
            createdTodo => {
              console.log('Todo created', createdTodo);
              this.todoService.todosChanged.emit();
            }, 
            error => {console.log('Error creating todo', error);});
        });
      }
    });
  }
}

@Component({
  selector: 'dialog-component-dialog',
  templateUrl: 'dialog-component-dialog.html',
})
export class DialogComponentDialog {
  constructor(
    public dialogRef: MatDialogRef<DialogComponentDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
}
