import { OnInit, Component, Input } from '@angular/core';
import { TodoService } from '../todo.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css']
})
export class SidenavComponent implements OnInit{
  todos: any[] = [];

  constructor(
    private todoService: TodoService,
    private router: Router
    ) {}

  ngOnInit() {
    this.todoService.getTodos().subscribe(todos => {
      this.todos = todos;
    });
  
    this.todoService.todosChanged.subscribe(() => {
      console.log('todosChanged event received');
      this.todoService.getTodos().subscribe(todos => {
        this.todos = todos;
      });
    });
  }

  onSelect(todo: any) {
    this.router.navigate(['/edit', todo.id])
  }
}
